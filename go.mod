module gitee.com/soundsee/lindu-utils

go 1.19

require (
	github.com/gofrs/uuid v4.4.0+incompatible
	github.com/golang-jwt/jwt/v4 v4.5.0
	github.com/mozillazg/go-pinyin v0.20.0
	golang.org/x/crypto v0.9.0
)

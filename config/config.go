package config

const (
	TimeZone                       = "Asia/Shanghai"
	TimeFormat                     = "2006-01-02 15:04:05"
	TimeFormatUnix                 = "2006-01-02 15:04:05.999999"
	RandLength                     = 6
	NoDataFind                     = "no rows"
	ExpiresString                  = "is expired"
	AccountUsernameLimit           = 24
	AccountDefaultTokenExpiredTime = 86400
	PageSizeDefault                = 10
	PageStartDefault               = 1
	CommonReplaceString            = "REPLACE"
	CommonDeleteCommonValue        = 1
	CommonDeleteValue              = 2
	CommonHandlerAdd               = "add"
	CommonHandlerEdit              = "edit"

	JWTPublicKeyE = "AQAB"
	JWTPublicKeyN = "yokACes81u2Q-8OvzNf1HJ1tP6egLNCrRJzs1aUMsXZqa-LOLFPn7hHrMOl9msC504WcvLYfKtet6tFQMd2VIqnUEOysOS_o5V82BBRDgZSOqT8--PXxQ1yirkzoYA-XvfvZtBqPbz8T1Igo4GM2Yt2rVv3ARpqIx8Si000Rbds4EU58uqs_ktdQ3xi694GGdG1LUw4FXL8c6AjaWNAFSWvZkWf54eE5ef9ylASOjm7MRJyDKfqbpE-4oO8uRiUcuhLQFG13pEeUl4ng4kpVywu_gtiBBX1uEzHep8S_OyPApMsbBKEF2goRFiAefev-PftGLTvZw7lulwWv6Xq1pw"

	JWTPublicKeySE = "AQAB"
	JWTPublicKeySN = "pZSemvTqg_l-qNDPQxZ92RyaDsvbMtSUyRMKi9rqyBpBhoWN1i4vSNZqJX6QUakKlqGeDebXFnELOzylzIuDbreGkr_upNLc4xIFkdXSDKxoxFqLECXxdQI5aQqY5-0_rfedabM2lIsoPaB8Zl8qdp50PNxjvAvAfkEs3OJ4h_c"
)

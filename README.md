# lindu-utils

#### 介绍

临都utils

#### 软件架构

无

#### 安装教程

go get gitee.com/soundsee/lindu-utils

#### 使用说明

1. utils.NowTimeReturn() 返回当前时间 2022-05-20 12:12:15
2. utils.RegexMobile(mobileValue) 手机号格式是否正确
3. utils.RegexPwd(pwdString) 密码格式是否正确
4. utils.RandString(int) 输出指定长度的随机字符串
5. utils.MD5Sign(signString) 输出指定字符串的MD5值
6. utils.MD5Sign16(signString) 输出指定字符串的16位MD5值
7. utils.UuidGenerate() 生成uuid
8. utils.RandNumberString(int) 输出指定长度的数字字符串
9. utils.PasswordBcrypt(password) 输出密码的加密字符串
10. utils.PasswordCompare(bcryptPassword,password) 验证密码是否正确
11. utils.StringsIsExists(parentString,childString) 字符串是否是某一字符串的子集
12. utils.GetFirstChar(string) 输出首字母
13. utils.PageLimit(pageStart,pageSize) 分页值计算
14. analytic.ParseNewJwtToken(token) 验证token是否正确，返回claims，用于LinDu服务端token验证
15. analytic.ParseNewJwtToken(token) 验证token是否正确，返回claims，用于LinDu服务端token验证

#### 参与贡献

1.  Seed Tian

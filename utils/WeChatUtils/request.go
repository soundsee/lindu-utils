package WeChatUtils

import (
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
	"encoding/json"
	"gitee.com/soundsee/lindu-utils/utils"
	"log"
)

// MiniAppOpenData 解析微信小程序code获取开放数据
func MiniAppOpenData(code, appKey, appSecret string) (returnB bool, data utils.MiniAppResponseData) {
	var wechatJson utils.MiniAppRequestData
	headerList := make(map[string]string)
	getResult, err := utils.CurlGet("https://api.weixin.qq.com/sns/jscode2session?appid="+appKey+"&secret="+appSecret+"&js_code="+code+"&grant_type=authorization_code", headerList)
	if err != nil {
		log.Printf("LinDuUtilsMiniAppGetResultError,error is %v", err.Error())
		return false, data
	}
	log.Printf("LinDuUtilsMiniAppRequestResponseBody,data is %v", getResult)
	err = json.Unmarshal([]byte(getResult), &wechatJson)
	if err != nil {
		log.Printf("LinDuUtilsMiniAppUnmarshalError,error is %v", err.Error())
		return false, data
	}

	if wechatJson.ErrCode != 0 {
		log.Printf("LinDuUtilsMiniAppResponseCodeError,code is %v,error msg is %v", wechatJson.ErrCode, wechatJson.ErrMsg)
		return false, data
	}

	data.OpenId = wechatJson.OpenId
	data.UnionId = wechatJson.UnionId
	data.SessionKey = wechatJson.SessionKey
	return true, data
}

// MiniAppEncryptDataParse 小程序手机号加密数据解析
func MiniAppEncryptDataParse(encryptedData, sessionKey, iv string) ([]byte, error) {
	aesKey, err := base64.StdEncoding.DecodeString(sessionKey)
	if err != nil {
		log.Printf("LinDuUtilsParseSessionKeyDecodeError,error is %v", err.Error())
		return nil, err
	}
	cipherText, err := base64.StdEncoding.DecodeString(encryptedData)
	if err != nil {
		log.Printf("LinDuUtilsParseEncryptDataDecodeError,error is %v", err.Error())
		return nil, err
	}
	ivBytes, err := base64.StdEncoding.DecodeString(iv)
	if err != nil {
		log.Printf("LinDuUtilsParseIvDecodeError,error is %v", err.Error())
		return nil, err
	}
	block, err := aes.NewCipher(aesKey)
	if err != nil {
		log.Printf("LinDuUtilsAesBlockError,error is %v", err.Error())
		return nil, err
	}
	mode := cipher.NewCBCDecrypter(block, ivBytes)
	mode.CryptBlocks(cipherText, cipherText)
	length := len(cipherText)
	unp := int(cipherText[length-1])
	return cipherText[:(length - unp)], nil
}

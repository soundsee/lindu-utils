package utils

const (
	RegexForMobile   = `^1\d{10}$`
	RegexForPassword = "^[a-zA-Z\\d_`~!@#$%^&*()_\\-+=<>?:\"{}|,.\\/;'\\\\[\\]·~！@#￥%……&*（）——\\-+={}|《》？：“”【】、；‘’，。、]{6,50}$"
)

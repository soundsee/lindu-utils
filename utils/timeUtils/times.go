package timeUtils

import (
	"gitee.com/soundsee/lindu-utils/config"
	"time"
)

// ParseFormatTimeToUnix 解析string类型时间到unix
func ParseFormatTimeToUnix(str string) (unixTime int64) {
	var location, err = time.LoadLocation("Asia/Shanghai")
	if err != nil {
		return
	}
	t, err := time.ParseInLocation(config.TimeFormat, str, location)
	if err == nil {
		return t.Unix()
	}
	return unixTime
}

// ParseFormatTimeToUnixNano 解析string类型时间到unixNano
func ParseFormatTimeToUnixNano(str string) (unixTime int64) {
	var location, err = time.LoadLocation("Asia/Shanghai")
	if err != nil {
		return
	}
	t, err := time.ParseInLocation(config.TimeFormat, str, location)
	if err == nil {
		return t.UnixNano()
	}
	return unixTime
}

package utils

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"
)

const (
	CommonRequestSuccess = "success"
	CommonRequestError   = "error"
	CommonRequestTimeout = 5
)

// CurlGet Get请求
// @Param	Url			request url
// @Param	headerList	Header list
func CurlGet(url string, headerList map[string]string) (string, error) {
	client := &http.Client{
		Timeout: time.Duration(CommonRequestTimeout) * time.Second,
	}
	request, err := http.NewRequest("GET", url, nil)

	if err != nil {
		return "", errors.New(CommonRequestSuccess + ":" + err.Error())
	}
	for headerKey, headerVal := range headerList {
		request.Header.Add(headerKey, headerVal)
	}

	response, err := client.Do(request)

	if err != nil {
		return "", errors.New(CommonRequestError + ":" + err.Error())
	}
	defer func() {
		_ = response.Body.Close()
	}()
	result, _ := io.ReadAll(response.Body)

	return string(result), nil
}

// CurlPost Post request
// @Param	url			string
// @Param	headerList	map
// @Param	data		interface
// @Param	isJson		bool
func CurlPost(url string, headerList map[string]string, data interface{}, isJson bool) (string, error) {
	client := &http.Client{
		Timeout: time.Duration(CommonRequestTimeout) * time.Second,
	}
	var jsonBody interface{}
	if !isJson {
		jsonBody, _ = json.Marshal(data)
	} else {
		jsonBody = data
	}
	req, err := http.NewRequest("POST", url, strings.NewReader(fmt.Sprint(jsonBody)))
	if err != nil {
		return "", errors.New(CommonRequestError + ":" + err.Error())
	}
	for headerKey, headerVal := range headerList {
		req.Header.Add(headerKey, headerVal)
	}
	response, err := client.Do(req)
	if err != nil {
		return "", errors.New(CommonRequestError + ":" + err.Error())
	}
	defer func() {
		err = response.Body.Close()
	}()

	result, _ := io.ReadAll(response.Body)
	return string(result), nil
}

// CurlRequest	Request curl
func CurlRequest(method, url string, headerList map[string]string, data interface{}) (string, error) {
	client := &http.Client{
		Timeout: time.Duration(CommonRequestTimeout) * time.Second,
	}
	jsonBody, _ := json.Marshal(data)
	request, err := http.NewRequest(method, url, bytes.NewBuffer(jsonBody))

	if err != nil {
		return "", errors.New(CommonRequestSuccess + ":" + err.Error())
	}
	for headerKey, headerVal := range headerList {
		request.Header.Add(headerKey, headerVal)
	}
	response, err := client.Do(request)

	if err != nil {
		return "", errors.New(CommonRequestError + ":" + err.Error())
	}
	defer func() {
		_ = response.Body.Close()
	}()

	result, err := io.ReadAll(response.Body)
	return string(result), err
}

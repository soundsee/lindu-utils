package utils

// MiniAppRequestData 请求WeChat PlatForm返回数据结构
type MiniAppRequestData struct {
	OpenId     string `json:"openid"`
	SessionKey string `json:"session_key"`
	UnionId    string `json:"unionid"`
	ErrCode    int    `json:"errcode"`
	ErrMsg     string `json:"errmsg"`
}

// MiniAppResponseData 微信小程序返回数据
type MiniAppResponseData struct {
	OpenId     string `json:"openId"`
	UnionId    string `json:"unionId"`
	SessionKey string `json:"sessionKey"`
}

package utils

import (
	"crypto/md5"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"gitee.com/soundsee/lindu-utils/config"
	"github.com/gofrs/uuid"
	"github.com/mozillazg/go-pinyin"
	"golang.org/x/crypto/bcrypt"
	"math/rand"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"
)

// NowTimeReturn 返回当前时间 格式:2006-01-02 15:04:05
func NowTimeReturn() string {
	var timezone, _ = time.LoadLocation(config.TimeZone)
	return time.Now().In(timezone).Format(config.TimeFormat)
}

// RegexMobile 验证手机号格式是否正确
func RegexMobile(mobile string) (re bool) {
	defer func() {
		recover()
	}()
	regexCompile := regexp.MustCompile(RegexForMobile)
	regexCompile.Longest()
	re = regexCompile.MatchString(mobile)
	return
}

// RegexPwd 正则验证，密码长度和格式是否正确
func RegexPwd(bindPwd string) (cV bool) {
	defer func() {
		recover()
	}()
	regexCompile := regexp.MustCompile(RegexForPassword)
	regexCompile.Longest()
	cV = regexCompile.MatchString(bindPwd)
	return
}

// RandString 生成随机字符串
func RandString(n uint8) string {
	str := "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	bytesData := []byte(str)
	var result []byte
	rand.Seed(time.Now().UnixNano() + int64(rand.Intn(100)))
	var i uint8
	for i = 0; i < n; i++ {
		result = append(result, bytesData[rand.Intn(len(bytesData))])
	}
	return string(result)
}

// MD5Sign MD5加密
func MD5Sign(signString string) (s string) {
	h := md5.New()
	h.Write([]byte(signString))
	cipherStr := h.Sum(nil)
	s = hex.EncodeToString(cipherStr)
	return s
}

// MD5Sign16 MD5加密16位
func MD5Sign16(signString string) (s string) {
	s = MD5Sign(signString)
	return s[8:24]
}

// UuidGenerate uuid生成器
func UuidGenerate() (s string) {
	u2, err := uuid.NewV4()
	if err != nil {
		return
	}
	s = base64.StdEncoding.EncodeToString([]byte(fmt.Sprint(u2)))
	return s
}

// RandNumberString 生成纯数字字符串
func RandNumberString(length int) string {
	numeric := [10]byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	r := len(numeric)
	rand.Seed(time.Now().UnixNano())

	var sb strings.Builder
	for i := 0; i < length; i++ {
		_, err := fmt.Fprintf(&sb, "%d", numeric[rand.Intn(r)])
		if err != nil {
			return ""
		}
	}
	return sb.String()
}

// PasswordBcrypt 密码加密
func PasswordBcrypt(password string) (rePwd string, b bool) {
	bytesPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return rePwd, false
	}
	return string(bytesPassword), true
}

// PasswordCompare 密码比较
func PasswordCompare(bcryptPwd, pwd string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(bcryptPwd), []byte(pwd))
	if err != nil {
		return false
	}
	return true
}

// StringsIsExists 字符串是否存在
func StringsIsExists(p, c string) bool {
	if strings.Index(p, c) > -1 {
		return true
	}
	return false
}

// GetFirstChar 获取首字母
func GetFirstChar(chars string) string {
	args := pinyin.NewArgs()
	args.Style = pinyin.FirstLetter
	result := pinyin.Pinyin(chars, args)
	if len(result) == 0 {
		return ""
	}
	return strings.ToUpper(result[0][0])
}

// PageLimit 分类limit值
func PageLimit(pageStart, pageSize int) string {
	if pageStart == 0 {
		pageStart = config.PageStartDefault
	}
	if pageSize == 0 {
		pageSize = config.PageSizeDefault
	}
	return " limit " + fmt.Sprint((pageStart-1)*pageSize) + "," + fmt.Sprint(pageSize)
}

// UsedTimeMonthDayHourMinute 订单生成时间戳
func UsedTimeMonthDayHourMinute() string {
	var usedTime string
	usedTime = time.Now().Format("01") + fmt.Sprint(time.Now().Day()) + time.Now().Format("15") + time.Now().Format("04")
	return usedTime
}

// UniqueArr 去除共同项
func UniqueArr(arr []int) (result []int) {
	sort.Ints(arr)
	arrLen := len(arr)
	for i := 0; i < arrLen; i++ {
		if i < arrLen-1 && arr[i] != arr[i+1] {
			result = append(result, arr[i])
		}
	}
	result = append(result, arr[arrLen-1])
	return result
}

// SortStrings 字符串排序
func SortStrings(s, sep string) string {
	slices := strings.Split(s, sep)
	sort.Strings(slices)
	return strings.Join(slices, sep)
}

// SortSpecAttrStrings 商品规格排序-规避字符串中，数字大的规格ID在前面
func SortSpecAttrStrings(s, sep string) string {
	slices := strings.Split(s, sep)
	intArr := SliceStringToInt(slices)
	sort.Ints(intArr)
	str := SliceIntToString(intArr)
	return strings.Join(str, sep)
}

// SliceIntToString int类型数组转换string类型
func SliceIntToString(data []int) []string {
	var str []string
	for _, val := range data {
		str = append(str, fmt.Sprint(val))
	}
	return str
}

// SliceStringToInt string类型数组转换int类型
func SliceStringToInt(data []string) []int {
	var intArr []int
	for _, val := range data {
		i, err := strconv.Atoi(val)
		if err == nil {
			intArr = append(intArr, i)
		}
	}
	return intArr
}

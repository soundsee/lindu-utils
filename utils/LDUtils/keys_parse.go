package LDUtils

import (
	"bytes"
	"crypto/rsa"
	"encoding/base64"
	"encoding/binary"
	"errors"
	"github.com/golang-jwt/jwt/v4"
	"log"
	"math/big"
)

// GetNAndEFromPublicKey 从公钥中读取N和E
func GetNAndEFromPublicKey(publicKey string) (n, e string) {
	// 获取解析token的公钥
	SecretKey := "-----BEGIN CERTIFICATE-----\n" + publicKey + "\n-----END CERTIFICATE-----"
	key, err := jwt.ParseRSAPublicKeyFromPEM([]byte(SecretKey))
	if err != nil {
		log.Printf("LDUtilsParsePublicKeyFromPEMError,error is %v", err.Error())
		return n, e
	}
	bs := make([]byte, 4)
	binary.BigEndian.PutUint32(bs, uint32(key.E))
	bs = bs[1:]
	e = base64.URLEncoding.EncodeToString(bs)
	n = base64.RawURLEncoding.EncodeToString(key.N.Bytes())
	return n, e
}

// GetRsaPublicKeyFromNAndE 从N和E中读取RSA的公钥信息
func GetRsaPublicKeyFromNAndE(keysN, keysE string) *rsa.PublicKey {
	decN, err := base64.RawURLEncoding.DecodeString(keysN)
	if err != nil {
		log.Printf("DecodeStringPublicKeyNError,error is %v", err.Error())
		return nil
	}

	n := big.NewInt(0)
	n.SetBytes(decN)

	decE, err := base64.StdEncoding.DecodeString(keysE)
	if err != nil {
		log.Printf("DecodeStringPublicKeyEError,error is %v", err.Error())
		return nil
	}
	var eBytes []byte
	if len(decE) < 8 {
		eBytes = make([]byte, 8-len(decE), 8)
		eBytes = append(eBytes, decE...)
	} else {
		eBytes = decE
	}
	eReader := bytes.NewReader(eBytes)
	var e uint64
	err = binary.Read(eReader, binary.BigEndian, &e)
	if err != nil {
		log.Printf("BinaryReadPublicKeyEError,error is %v", err.Error())
		return nil
	}
	pKey := rsa.PublicKey{N: n, E: int(e)}
	return &pKey
}

// ParseJwtTokenFromPublicKey 通过公钥解析jwt token
// t == jwt token
// publicKey == 用来解析jwt token的公钥信息
func ParseJwtTokenFromPublicKey(t string, publicKey *rsa.PublicKey) (jwt.MapClaims, error) {
	// 获取解析token的公钥
	var returnData jwt.MapClaims
	if publicKey == nil {
		return returnData, errors.New("public key error")
	}
	token, err := jwt.Parse(t, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, errors.New("unexpected signing method")
		}
		return publicKey, nil
	})
	if err != nil {
		return returnData, err
	}
	// 设置上下文值
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		return claims, nil
	} else {
		return returnData, errors.New("claims error")
	}
}

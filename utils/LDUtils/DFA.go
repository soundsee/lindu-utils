package LDUtils

const (
	InitTrieChildrenNum = 128
)

type trieNode struct {
	isEndOfWord bool
	children    map[rune]*trieNode
}

func newTrieNode() *trieNode {
	return &trieNode{
		isEndOfWord: false,
		children:    make(map[rune]*trieNode, InitTrieChildrenNum),
	}
}

// Match index object
type matchIndex struct {
	start int // start index
	end   int // end index
}

// Construct from scratch
func newMatchIndex(start, end int) *matchIndex {
	return &matchIndex{
		start: start,
		end:   end,
	}
}

// DFAUtil dfa util
type DFAUtil struct {
	root *trieNode
}

func (d *DFAUtil) insertWord(word []rune) {
	currNode := d.root
	for _, c := range word {
		if childNode, exist := currNode.children[c]; !exist {
			childNode = newTrieNode()
			currNode.children[c] = childNode
			currNode = childNode
		} else {
			currNode = childNode
		}
	}

	currNode.isEndOfWord = true
}

// Check if there is any word in the trie that starts with the given prefix.
func (d *DFAUtil) startsWith(prefix []rune) bool {
	currNode := d.root
	for _, c := range prefix {
		if childNode, exist := currNode.children[c]; !exist {
			return false
		} else {
			currNode = childNode
		}
	}

	return true
}

// searchWord and make sure if a word is existed in the underlying trie.
func (d *DFAUtil) searchWord(word []rune) bool {
	currNode := d.root
	for _, c := range word {
		if childNode, exist := currNode.children[c]; !exist {
			return false
		} else {
			currNode = childNode
		}
	}

	return currNode.isEndOfWord
}

// searchSentence a whole sentence and get all the matching words and their indices
func (d *DFAUtil) searchSentence(sentence string) (matchIndexList []*matchIndex) {
	start, end := 0, 1
	sentenceRuneList := []rune(sentence)

	// Iterate the sentence from the beginning to the end.
	startsWith := false
	for end <= len(sentenceRuneList) {
		// Check if a sensitive word starts with word range from [start:end)
		// We find the longest possible path
		// Then we check any sub word is the sensitive word from long to short
		if d.startsWith(sentenceRuneList[start:end]) {
			startsWith = true
			end += 1
		} else {
			if startsWith == true {
				// Check any sub word is the sensitive word from long to short
				for index := end - 1; index > start; index-- {
					if d.searchWord(sentenceRuneList[start:index]) {
						matchIndexList = append(matchIndexList, newMatchIndex(start, index-1))
						break
					}
				}
			}
			start, end = end-1, end+1
			startsWith = false
		}
	}

	if startsWith {
		for index := end - 1; index > start; index-- {
			if d.searchWord(sentenceRuneList[start:index]) {
				matchIndexList = append(matchIndexList, newMatchIndex(start, index-1))
				break
			}
		}
	}

	return
}

// IsMatch Judge if input sentence contains some special
func (d *DFAUtil) IsMatch(sentence string) bool {
	return len(d.searchSentence(sentence)) > 0
}

// HandleWord Handle sentence. Use specified character to replace those sensitive characters.
// input: Input sentence
// replaceCh: candidate
// Return:
// Sentence after manipulation
func (d *DFAUtil) HandleWord(sentence string, replaceCh rune) string {
	matchIndexList := d.searchSentence(sentence)
	if len(matchIndexList) == 0 {
		return sentence
	}

	// Manipulate
	sentenceList := []rune(sentence)
	for _, matchIndexObj := range matchIndexList {
		for index := matchIndexObj.start; index <= matchIndexObj.end; index++ {
			sentenceList[index] = replaceCh
		}
	}

	return string(sentenceList)
}

// NewDFAUtil Create new DfaUtil object
// wordList:word list
func NewDFAUtil() *DFAUtil {
	this := &DFAUtil{
		root: newTrieNode(),
	}

	wordsList := SensitiveWords()
	for _, word := range wordsList {
		wordRuneList := []rune(word)
		if len(wordRuneList) > 0 {
			this.insertWord(wordRuneList)
		}
	}

	return this
}

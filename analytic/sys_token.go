package analytic

import (
	"bytes"
	"crypto/rsa"
	"encoding/base64"
	"encoding/binary"
	"errors"
	"gitee.com/soundsee/lindu-utils/config"
	"github.com/golang-jwt/jwt/v4"
	"log"
	"math/big"
)

// ParseSysJwtToken jwt token解析
func ParseSysJwtToken(t string) (map[string]interface{}, error) {
	// 获取解析token的公钥
	mapReturn := make(map[string]interface{})
	key := JwtPublicKeyGetS()
	if key == nil {
		return mapReturn, errors.New("public key error")
	}
	token, err := jwt.Parse(t, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, errors.New("unexpected signing method")
		}
		return key, nil
	})
	if err != nil {
		return mapReturn, err
	}
	// 设置上下文值
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		if claims["UUID"] != nil {
			mapReturn["uuid"] = claims["UUID"].(string)
		}
		if claims["Username"] != nil {
			mapReturn["username"] = claims["Username"].(string)
		}
		return mapReturn, nil
	} else {
		return mapReturn, errors.New("claims error")
	}
}

// JwtPublicKeyGetS 根据N和E生成RSA的公钥
func JwtPublicKeyGetS() *rsa.PublicKey {
	decN, err := base64.RawURLEncoding.DecodeString(config.JWTPublicKeySN)
	if err != nil {
		log.Printf("DecodeStringPublicKeyNError,error is %v", err.Error())
		return nil
	}

	n := big.NewInt(0)
	n.SetBytes(decN)

	decE, err := base64.StdEncoding.DecodeString(config.JWTPublicKeySE)
	if err != nil {
		log.Printf("DecodeStringPublicKeyEError,error is %v", err.Error())
		return nil
	}
	var eBytes []byte
	if len(decE) < 8 {
		eBytes = make([]byte, 8-len(decE), 8)
		eBytes = append(eBytes, decE...)
	} else {
		eBytes = decE
	}
	eReader := bytes.NewReader(eBytes)
	var e uint64
	err = binary.Read(eReader, binary.BigEndian, &e)
	if err != nil {
		log.Printf("BinaryReadPublicKeyEError,error is %v", err.Error())
		return nil
	}
	pKey := rsa.PublicKey{N: n, E: int(e)}
	return &pKey
}

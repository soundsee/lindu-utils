package analytic

import (
	"bytes"
	"crypto/rsa"
	"encoding/base64"
	"encoding/binary"
	"errors"
	"gitee.com/soundsee/lindu-utils/config"
	"github.com/golang-jwt/jwt/v4"
	"log"
	"math/big"
)

// ParseNewJwtToken jwt token解析
func ParseNewJwtToken(t string) (map[string]interface{}, error) {
	// 获取解析token的公钥
	mapReturn := make(map[string]interface{})
	key := JwtPublicKeyGet()
	if key == nil {
		return mapReturn, errors.New("public key error")
	}
	token, err := jwt.Parse(t, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, errors.New("unexpected signing method")
		}
		return key, nil
	})
	if err != nil {
		return mapReturn, err
	}
	// 设置上下文值
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		if claims["openId"] != nil {
			mapReturn["openId"] = claims["openId"].(string)
		}
		if claims["env"] != nil {
			mapReturn["env"] = claims["env"].(string)
		}
		if claims["sub"] != nil {
			mapReturn["sub"] = claims["sub"].(string)
		}
		return mapReturn, nil
	} else {
		return mapReturn, errors.New("claims error")
	}
}

// JwtPublicKeyGet 根据N和E生成RSA的公钥
func JwtPublicKeyGet() *rsa.PublicKey {
	decN, err := base64.RawURLEncoding.DecodeString(config.JWTPublicKeyN)
	if err != nil {
		log.Printf("DecodeStringPublicKeyNError,error is %v", err.Error())
		return nil
	}

	n := big.NewInt(0)
	n.SetBytes(decN)

	decE, err := base64.StdEncoding.DecodeString(config.JWTPublicKeyE)
	if err != nil {
		log.Printf("DecodeStringPublicKeyEError,error is %v", err.Error())
		return nil
	}
	var eBytes []byte
	if len(decE) < 8 {
		eBytes = make([]byte, 8-len(decE), 8)
		eBytes = append(eBytes, decE...)
	} else {
		eBytes = decE
	}
	eReader := bytes.NewReader(eBytes)
	var e uint64
	err = binary.Read(eReader, binary.BigEndian, &e)
	if err != nil {
		log.Printf("BinaryReadPublicKeyEError,error is %v", err.Error())
		return nil
	}
	pKey := rsa.PublicKey{N: n, E: int(e)}
	return &pKey
}
